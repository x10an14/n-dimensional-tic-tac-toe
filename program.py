from collections import OrderedDict
from contextlib import suppress
from random import randrange
from sys import argv
from typing import List, Tuple, Iterable, Set


def n_dimensional_tictactoe(n: int) -> List[int]:
    if 1 >= n:
        return [0] * 3
    return [
        n_dimensional_tictactoe(n - 1)
        for _ in
        range(3)
    ]


def select_random_coordinates(
    n_dimensional_matrix: Iterable[int],
) -> Tuple[int]:
    count, dimensions_size = 0, OrderedDict()
    while isinstance(n_dimensional_matrix, list):
        dimensions_size[count], count = len(n_dimensional_matrix), count + 1
        n_dimensional_matrix = n_dimensional_matrix[0]
    return tuple([
        randrange(0, size)
        for _, size in
        dimensions_size.items()
    ])


def fill_coordinate_slots(
    coordinates: Iterable[Iterable[int]],
    matrix: List[int],
    *,
    fill_value: int = 1,
) -> List[int]:
    for coords in coordinates:
        *coords, last_coordinate = coords
        current_dimension = matrix
        for dimension in coords:
            current_dimension = current_dimension[dimension]
        current_dimension[last_coordinate] = 1
    return matrix


def generate_n_random_coordinates(
    n: int,
    board: Iterable[int]
) -> Set[Tuple[int]]:
    set_of_coordinates = set()
    while len(set_of_coordinates) < n:
        set_of_coordinates.add(select_random_coordinates(board))
    return set_of_coordinates


def generate_vector(
    coordinate1: Iterable[int],
    coordinate2: Iterable[int],
) -> Tuple[int]:
    try:
        error_message = f"coordinate1 (size {len(coordinate1)})"
        error_message += f" and coordinate2 (size {len(coordinate2)})"
        error_message += ' must be of equal length.'
        assert len(coordinate1) == len(coordinate2), error_message
    except AssertionError as e:
        raise ValueError(error_message)

    vector = map(
        lambda coords_tuple: coords_tuple[0] - coords_tuple[1],
        zip(coordinate1, coordinate2)
    )
    return tuple(vector)


def add_vector_to_coordinate(
    coordinate: Iterable[int],
    vector: Iterable[int],
) -> Tuple[int]:
    try:
        error_message = f"coordinate (size {len(coordinate)})"
        error_message += f" and vector (size {len(vector)})"
        error_message += ' must be of equal length.'
        assert len(coordinate) == len(vector), error_message
    except AssertionError as e:
        raise ValueError(error_message)

    new_coordinate = map(
        lambda coords_tuple: coords_tuple[0] + coords_tuple[1],
        zip(coordinate, vector)
    )
    return tuple(new_coordinate)


def largest_vector_delta(
    vector: Iterable[int],
) -> int:
    deltas = map(abs, vector)
    return max(deltas)


def is_tictactoe_won(filled_slot_coordinates: Set[int]) -> bool:
    try:
        assert len(filled_slot_coordinates) == 3
    except AssertionError:
        raise NotImplementedError(
            "is_tictactoe_won() only supports three coordinates."
        )
    coord1, coord2, coord3 = tuple(filled_slot_coordinates)
    vector12 = generate_vector(coord1, coord2)
    if largest_vector_delta(vector12) == 1:
        if (
            add_vector_to_coordinate(
                coord1,
                vector12
            ) == coord3 or
            add_vector_to_coordinate(
                coord3,
                vector12,
            ) == coord2
        ):
            return True

    vector13 = generate_vector(coord1, coord3)
    if largest_vector_delta(vector13) == 1:
        if (
            add_vector_to_coordinate(
                coord1,
                vector13
            ) == coord2 or
            add_vector_to_coordinate(
                coord2,
                vector13
            ) == coord3
        ):
            return True
    return False


if __name__ == '__main__':
    number_of_dimensions = 3
    with suppress(IndexError, ValueError):
        number_of_dimensions = int(argv[1])
    iterations = 10**5
    with suppress(IndexError, ValueError):
        iterations = int(argv[2])

    wins, data = 0, dict()
    for iteration in range(iterations):
        board = n_dimensional_tictactoe(number_of_dimensions)
        coordinates = frozenset(generate_n_random_coordinates(3, board))
        # board = fill_coordinate_slots(
        #     coordinates=coordinates,
        #     matrix=board
        # )
        del board
        data[iteration] = dict(
            coordinates=coordinates,
            is_game_won=is_tictactoe_won(coordinates),
        )
        wins += 1 if data[iteration]['is_game_won'] else 0
        if iteration % 10**6 == 0 or iteration % 500_000 == 0:
            print(
                f"Iteration {iteration:,}: {coordinates} "
                f"-> Won? {data[iteration]['is_game_won']}"
            )

    print(f"There were a total of {wins} wins in {iterations} amount of games.")
    print(f"\tThis makes {(wins/iterations)*100:.2f}% win-rate.")
